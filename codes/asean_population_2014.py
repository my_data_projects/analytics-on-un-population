import read_data
import matplotlib.pyplot as plt

Asean_countries = ['Brunei Darussalam', 'Cambodia', 'Indonesia',
                   "Lao People's Democratic Republic", 'Malaysia',
                   'Myanmar', 'Philippines', 'Singapore', 'Thailand',
                   'Viet Nam']


def get_Asean_population_2014(file):
    data = read_data.read_data(file)
    Asean_population_2014 = {}
    for row in data:
        if row['Region'] in Asean_countries and row['Year'] == '2014':
            population = int(float(row['Population']))
            Asean_population_2014[row['Region']] = population
    Asean_population_2014 = dict(sorted(Asean_population_2014.items(),
                                 key=lambda x: x[1]))
    return Asean_population_2014


def plot_Bar(file):
    data = get_Asean_population_2014(file)
    countries = list(data.keys())
    population = list(data.values())
    # creating the bar plot
    plt.gcf().autofmt_xdate()
    plt.bar(countries, population[0:5000])
    plt.xlabel("countries")
    plt.title("Asean countries population over years")
    plt.show()


def execution_Asean_countries_population_2014(file):
    plot_Bar(file)


file = 'population-estimates.csv'
execution_Asean_countries_population_2014(file)
