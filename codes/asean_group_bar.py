import read_data
import matplotlib.pyplot as plt

Asean_countries = ['Brunei Darussalam', 'Cambodia', 'Indonesia',
                   "Lao People's Democratic Republic", 'Malaysia',
                   'Myanmar', 'Philippines', 'Singapore', 'Thailand',
                   'Viet Nam']


def get_asean_population_2004_2014(file):
    data = read_data.read_data(file)
    Asean_population_year_wise = {}
    for row in data:
        if int(row['Year']) in range(2004, 2015):
            Asean_population_year_wise[row['Year']] = {}
    for row in data:
        if int(row['Year']) in range(2004, 2015):
            if row['Region'] in Asean_countries:
                population = float(row['Population'])
                if row['Year'] in Asean_population_year_wise.keys():
                    Asean_population_year_wise[row['Year']][row['Region']]= population
                else:
                    Asean_population_year_wise[row['Year']] = {row['Region']: population}
            else:
                pass
    return Asean_population_year_wise


def group_plot(data):
    x = range(10)
    y = -0.2
    width = 0.2
    years = []
    for year in sorted(data.keys()):
        years.append(year)
        z = list(map(lambda a: a + y, x))
        plt.bar(z, data[year].values(), width)
        y += 0.05
    plt.legend(years)
    plt.xticks(x, Asean_countries, rotation=30)
    plt.xlabel("Years")
    plt.ylabel("Population")
    plt.show()


if __name__ == '__main__':
    csv = 'population-estimates.csv'
    data = get_asean_population_2004_2014(csv)
    group_plot(data)
