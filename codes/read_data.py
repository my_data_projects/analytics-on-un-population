import csv


def read_data(file):
    # Reading Data from csv
    with open(file, 'r') as f:
        csv_reader = csv.DictReader(f)
        population_data_list = list(csv_reader)
    return population_data_list
