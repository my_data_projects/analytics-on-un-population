import read_data
import matplotlib.pyplot as plt


def get_Indian_population(file):
    data = read_data.read_data(file)
    indian_population_over_years = {}
    for row in data:
        if row['Region'] == 'India':
            indian_population_over_years[row['Year']] = row['Population']
    return indian_population_over_years


def plot_Bar_chart(file):
    data = get_Indian_population(file)
    years = list(data.keys())
    population = list(data.values())
    # creating the bar plot
    plt.gcf().autofmt_xdate()
    plt.bar(years, population[0:30000])
    plt.xlabel("years")
    plt.title("Indias population over years")
    plt.show()


def execution_population_data_india(file):
    plot_Bar_chart(file)


file = 'population-estimates.csv'
execution_population_data_india(file)