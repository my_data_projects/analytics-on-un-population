import read_data
import matplotlib.pyplot as plt

Saarc_countries = ['Brunei', 'Cambodia', 'Indonesia', 'Laos', 'Malaysia',
                   'Myanmar', 'the Philippines', 'Singapore',
                   'Thailand and Vietnam']


def get_saarc_year_wise_population(file):
    data = read_data.read_data(file)
    Saarc_population = {}
    for row in data:
        if row['Region'] in Saarc_countries:
            if row['Year'] in Saarc_population:
                Saarc_population[row['Year']] += int(float(row['Population']))
            else:
                Saarc_population[row['Year']] = int(float(row['Population']))
    return Saarc_population


def plot_bar(file):
    data = get_saarc_year_wise_population(file)
    years = list(data.keys())
    population = list(data.values())
    # creating the bar plot
    plt.gcf().autofmt_xdate()
    plt.bar(years, population[0:5000], color='maroon')
    plt.xlabel("years")
    plt.title("year wise Saarc countries Population")
    plt.show()


def execution_saarc_year_wise_population(file):
    plot_bar(file)


file = 'population-estimates.csv'
execution_saarc_year_wise_population(file)
