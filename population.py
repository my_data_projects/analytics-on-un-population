import csv
import matplotlib.pyplot as plt


def read_data(file):
    # Reading Data from csv
    with open(file, 'r') as f:
        csv_reader = csv.DictReader(f)
        population_data_list = list(csv_reader)
    return population_data_list


def get_Indian_population(file):
    data = read_data(file)
    indian_population_over_years = {}
    for row in data:
        if row['Region'] == 'India':
            indian_population_over_years[row['Year']] = row['Population']
    return indian_population_over_years


def plot_Bar_chart(file):
    data = get_Indian_population(file)
    years = list(data.keys())
    population = list(data.values())
    # creating the bar plot
    plt.gcf().autofmt_xdate()
    plt.bar(years, population[0:30000])
    plt.xlabel("years")
    plt.title("Indias population over years")
    plt.show()


def execution_population_data_india(file):
    plot_Bar_chart(file)

######################################################


Asean_countries = ['Brunei Darussalam', 'Cambodia', 'Indonesia',
                   "Lao People's Democratic Republic", 'Malaysia',
                   'Myanmar', 'Philippines', 'Singapore', 'Thailand',
                   'Viet Nam']


def get_Asean_population_2014(file):
    data = read_data(file)
    Asean_population_2014 = {}
    for row in data:
        if row['Region'] in Asean_countries and row['Year'] == '2014':
            population = int(float(row['Population']))
            Asean_population_2014[row['Region']] = population
    Asean_population_2014 = dict(sorted(Asean_population_2014.items(),
                                 key=lambda x: x[1]))
    return Asean_population_2014


def plot_Bar(file):
    data = get_Asean_population_2014(file)
    countries = list(data.keys())
    population = list(data.values())
    # creating the bar plot
    plt.gcf().autofmt_xdate()
    plt.bar(countries, population[0:5000])
    plt.xlabel("countries")
    plt.title("Asean countries population over years")
    plt.show()


def execution_Asean_countries_population_2014(file):
    plot_Bar(file)

###################################################


Saarc_countries = ['Brunei', 'Cambodia', 'Indonesia', 'Laos', 'Malaysia',
                   'Myanmar', 'the Philippines', 'Singapore',
                   'Thailand and Vietnam']


def get_saarc_year_wise_population(file):
    data = read_data(file)
    Saarc_population = {}
    for row in data:
        if row['Region'] in Saarc_countries:
            if row['Year'] in Saarc_population:
                Saarc_population[row['Year']] += int(float(row['Population']))
            else:
                Saarc_population[row['Year']] = int(float(row['Population']))
    return Saarc_population


def plot_bar(file):
    data = get_saarc_year_wise_population(file)
    years = list(data.keys())
    population = list(data.values())
    # creating the bar plot
    plt.gcf().autofmt_xdate()
    plt.bar(years, population[0:5000], color='maroon')
    plt.xlabel("years")
    plt.title("year wise Saarc countries Population")
    plt.show()


def execution_saarc_year_wise_population(file):
    plot_bar(file)


##############################################################

def get_Asean_population_2004_2014(file):
    data = read_data(file)
    Asean_population_year_wise = {}
    for row in data:
        if int(row['Year']) in range(2004, 2015):
            Asean_population_year_wise[row['Year']] = []
    for row in data:
        if int(row['Year']) in range(2004, 2015):
            if row['Region'] in Asean_countries:
                population = int(float(row['Population']))
                Asean_population_year_wise[row['Year']].append((row['Region'],
                                                                population))
            else:
                pass
    return Asean_population_year_wise

    def plot_Group_Bar_chart(file):
        data = get_Asean_population_2004_2014(file)
        population = list(data.values())
        countries = Asean_countries
        # creating the bar plot
        plt.gcf().autofmt_xdate()
        plt.bar(countries, population[0:5000], color='maroon')
        plt.xlabel("countries")
        plt.title("Asean countries population over years")


def execution_Asean_countries_population2004_2014(file):
    plot_Bar_chart(file)


#####################################################


def main():
    data_csv = 'analytics-on-un-population/population-estimates.csv'
    execution_population_data_india(data_csv)
    execution_Asean_countries_population_2014(data_csv)
    execution_saarc_year_wise_population(data_csv)
    execution_Asean_countries_population2004_2014(data_csv)


main()
